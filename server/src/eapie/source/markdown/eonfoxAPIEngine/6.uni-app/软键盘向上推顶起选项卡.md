在uni-app的 `manifest.json` 配置文件中，可配置设置项，固定选项卡（tabbar）至底部不被软键盘弹起。具体配置如下：

```
{
"app-plus":{
  "softinput" : {
            "mode" : "adjustPan"
        }
  }
}
```
