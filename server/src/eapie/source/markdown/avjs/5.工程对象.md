[TOC]
## 获取工程对象
```
av();//参数为空，获取的就是当前工程对象
av("test");//获取工程ID为“test”的工程对象
```
## 工程对象属性
|  键   |  名称 |  类型   |  描述 |
| --- | --- | --- | --- |
|   id  |   工程标识| String  |  不可被修改，不可被删除，所属工程对象的工程标识 |
|  selector |   设置筛选器/获取筛选节点 | Function  |  参数为空，是获取父节点。否则第一个参数传入的是节点筛选器或父节点DOM对象。如果第二个参数存在，并且为真值，那么表示该设置为全局修改 |
|  template  |   工程标识| Function  |  更新工程模板内容 |
|  main |   工程入口 | Function  |  不可被修改，不可被删除，详见工程文件的定义 |
|  event |   工程事件 | Object|  详见工程文件的定义 |
|  data |   渲染数据 | Object|  详见工程文件的定义 |
|  compiler |   编译控制 | Function  |  对工程的编译控制操作 |
|  render |   渲染控制 | Function  |  对工程的渲染控制操作 |
|  run |   运行控制 | Function  |  对工程的运行控制操作 |
|  state | 设置状态/获取状态信息 | Function  |  参数非空的时候是对工程的运行、渲染状态进行控制操作，返回工程对象。如果参数为空，返回的是该工程的状态信息 |
|  error |   错误信息 | Function  |  会触发公共事件与工程事件的错误事件函数 |
|  clone | 渲染数据拷贝  | Function  |  渲染数据非引用赋值 |
|  node | 节点列表  | Function  | 获取渲染后有序的节点列表Array。注意必须要先渲染 |
|  text | text内容 | Function  |  获取渲染后元素内容的文本。注意必须要先渲染 |
|  html | html内容  | Function  |  获取渲染后元素的html内容。注意必须要先渲染 |
|  ready | 初始化完成后才执行  | Function  |  传入一个函数，当工程初始化完成后才执行。注意，这个不是事件，只是一个执行队列 |
|  debug | 调试  | Function  |  打印一些私有的初始化参数信息 |
|  new | 实例新工程 | Function  |  由该工程分配一个新工程。成功返回新工程对象，否则失败返回false |
|  parent | 父工程 | Function   |  如果该工程是 new 出来的，那么就会存在父工程，返回值是父工程对象 |
|  children | 子工程 | Function   |  如果该工程 new了新工程，那么就会存在子工程，返回值是子工程列表Array |


## 工程渲染控制
注意，命令参数可以大小写，可以多个空格，可以传多个参数。
返回值是当前工程对象。
```
//以默认方式编译
av().render();
//是否强制刷新渲染值，默认false不强制
av().render("refresh");
//是否重新载入、强制清理容器，默认false不强制
av().render("reload");
//可以传多个参数
av().render("refresh","reload");
```

## 工程编译控制
注意，命令参数可以大小写，可以多个空格，可以传多个参数。
返回值是当前工程对象。
```
//以默认方式编译。没有编译则编译，否则不做任何操作
av().compiler();
//是否强制重新编译，默认false不强制
av().compiler("reload");
```
## 工程运行控制
注意，命令参数可以大小写，可以多个空格，可以传多个参数。
返回值是当前工程对象。
```
//以默认方式运行
av().run();
//是否以初始化的方式运行工程，默认false不初始化
av().run("init");
//是否强制刷新渲染值，默认false不强制
av().run("refresh");
//是否重新载入、强制清理容器，默认false不强制
av().run("reload");
//可以传多个参数
av().run("init","reload","refresh");
```
## 设置状态/获取状态信息 
注意，命令参数可以大小写，可以多个空格，可以传多个参数。
```
//修改为状态命令，可大小写都支持
//运行状态控制
av().state('run off'); //终止工程运行
av().state('run on'); //启动工程运行
av().state('run stop'); //停止(暂停)工程运行
//渲染状态控制
av().state('render off'); //终止工程渲染
av().state('render on'); //启动工程渲染
av().state('render stop'); //停止(暂停)工程渲染
//可以传多个参数
av().state('run on','render off') ;
```
返回值始终是一个记录该工程的状态信息的对象：
```
av().state();//支持参数为空，查看状态信息
```
|  键   |  类型   |  描述 |
| --- | --- |--- |
|  compilerCount  | Int |  编译统计次数  |
|  compilerState | Bool |  是否已经编译 |
|  renderCount | Int  | 渲染统计次数  |
|  renderState | String | 当前渲染状态字符串 |
|  renderStateOn | Bool | 当前渲染状态是否开启中 |
|  renderStateOff | Bool | 当前渲染状态是否已关闭 |
|  renderStateStop | Bool | 当前渲染状态是否已停止(暂停) |
|  runCount | Int  | 运行统计次数  |
|  runState | String | 当前运行状态字符串 |
|  runStateOn | Bool | 当前运行状态是否开启中 |
|  runStateOff | Bool | 当前运行状态是否已关闭 |
|  runStateStop | Bool | 当前运行状态是否已停止(暂停) |

## clone 渲染数据拷贝
注意，clone 该拷贝只对 基础类型 Object 与基础类型 Array 有效。
返回值是当前工程对象。
### 引用传值
意思是两个工程data数据交互时，av('project1').data赋给av('project2').data，那么av('project2').data与av('project1').data就是引用。av('project1').data或av('project2').data发生改变的时候，最终渲染的是av('project1')工程。如下代码：
```
var project2 = av('project2');
project2.data.option= av('project1').data.option;
```
### 非引用传值
av('project1').data拷贝给av('project2').data，实质是新复制了一份给对方。那么av('project1').data或av('project2').data发生改变的时候，渲染是他们自己。
```
var project2 = av('project2');
//拷贝
project2.clone({
    option: av('project1').data.option
});
```
## new 新工程
工程对象支持在本身情况下创建一个新工程，并且复制了一份他的工程参数。虽然两个工程有联系，但是彼此视图操作是不会相互受影响的。该功能实用于组件式开发，单个页面或多个页面对单个组件执行多次实例化，可在同个页面操作创建多个不受影响的相同组件。
如果工程ID参数为空，会默认一个工程ID：
```
var project = av("test-list").new();
//这时候就可以使用这个工程了
project.render();//渲染工程
console.log(project.id);//应该会打印出"test-list(1)"的默认工程ID
```
下面是指定实例化一个新工程：
```
var project = av("test-list").new("test-1");
//工程非默认，那么如果工程已经存在，会返回false
if( project ){
    //这时候就可以使用这个工程了
    av("test-1").render();//渲染工程
}
```
注意，在以某一个工程实例化新工程的时候，子工程只会初始化定义时的工程数据，不会复制父工程的，同样会重新执行import函数、执行ready工程事件。












