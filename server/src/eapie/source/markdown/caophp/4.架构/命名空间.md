<blockquote class="info"><p>命名空间的路径与类库文件的目录一致，那么就可以实现类的自动加载。</p></blockquote>

例如，在目录路径 application\controller\page 中的定义 session.php 类文件：
~~~
<?php
namespace application\controller\page;
class session {
	//...
}
~~~
如果我们实例化该类的话:
~~~
$session = new application\controller\page\session();
~~~
如果服务器根目录绝对路径为：C:\WWW。
那么，系统会自动加载。C:\WWW\\**application\controller\page\session.php** 路径文件。