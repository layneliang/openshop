<blockquote class="info"><p>框架语言操作方法，根据方法名称获取语言字符串。</p></blockquote>

* 全局赋值函数：
~~~
language(string $method[,mixed $args][,array $args1][,......]);
~~~

* 继承框架类：
~~~
framework\cao::language(string $method[,mixed $args][,array $args1][,......]);
~~~

| 参数类型  |  参数名称  |  参数备注  |   是否必须   |  传参顺序   |
| --- | --- | --- | --- | --- |
|  string  | $method  |  方法名称  |  是  |  mixed[0] |
|  mixed  |  $args  |  第1个参数  |  否  | mixed[1] |
|  mixed  |  ...  |  ...  |  ...  | mixed[...] |
|  mixed  |  $args+n  |  第n个参数  |  否  | mixed[n] |
