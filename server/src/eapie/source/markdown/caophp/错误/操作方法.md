<blockquote class="info"><p>错误方法可以捕获错误信息，并进行灵活操作。</p></blockquote>

##### 全局赋值函数：
~~~
error([closure $closure]);
~~~

##### 继承框架类：
~~~
framework\cao::error([closure $closure]);
~~~


| 参数类型  |  参数名称  |  参数备注  |   是否必须   |  传参顺序   |
| --- | --- | --- | --- | --- |
|closure|$closure|闭包函数，业务处理|否|closure[0]|

- $closure 接受1个参数，如function($info){}，$info即错误信息，该参数的与 error() 获取的数据是等价的。
- 如果error()参数为空，那么就是获取当前错误信息。如果错误不存在，则返回NULL。