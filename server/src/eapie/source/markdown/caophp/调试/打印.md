##### 全局赋值函数：
~~~
pre([mixed $data = NULL][bool $type = false]);
~~~

##### 继承框架类：
~~~
framework\cao::pre([mixed $data = NULL][bool $type = false]);
~~~

| 参数类型  |  参数名称  |  参数备注  |   是否必须   |  传参顺序   |
| --- | --- | --- | --- | --- |
|  mixed  |  $data  |  要打印的数据  |  否  |  mixed[0]  |
|  bool  |  $type  |  打印类型  |  否  | mixed[1]  |

- $data如果不是对象数据，那么$type类型为true，$data则以var_dump()格式输出。
- 该方法只能打印一个数据，并且程序不会终止。



```
$arr = array('人生至古谁无死', '留取丹心照汗青');
pre($arr);

//$type类型为true，$data则以var_dump()格式输出
pre($arr, true);
exit;
```



```
/* ******************** 打印结果 ******************** */
[Z:\WWW\website\localhost\index.php 所在 38 行]
Array
(
    [0] => 人生至古谁无死
    [1] => 留取丹心照汗青
)

[Z:\WWW\website\localhost\index.php 所在 40 行]
array(2) {
  [0]=>
  string(21) "人生至古谁无死"
  [1]=>
  string(21) "留取丹心照汗青"
}

```
